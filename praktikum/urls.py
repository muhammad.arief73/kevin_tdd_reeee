"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from website import views
from website.views import index, registration, activity
from django.conf.urls import url
from django.conf.urls.static import static

urlpatterns = [
    path('website/', include('website.urls')),
    path('admin/', admin.site.urls),
    path(r'index/', index, name = 'index'),
    path(r'activity/', activity, name = 'activity'),
    path(r'registration/', registration, name = 'registration'),
    url(r'^$', index, name='index'),
    url(r'^add_user_form_submission/$', views.add_user_form_submission, name='add_user_form_submission'),
    url(r'^add_schedule_form_submission/$', views.add_schedule_form_submission, name='add_schedule_form_submission'),
    url(r'^delete_schedules/$', views.delete_schedules, name='delete_schedules'),
]

