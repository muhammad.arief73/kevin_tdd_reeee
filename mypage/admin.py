from django.contrib import admin
from .models import Status

# Register your models here.
class StatusModelAdmin(admin.ModelAdmin):
    list_display = ["text","id"]
    list_filter = ["text", "id"]
    
    class Meta:
        model = Status
    
admin.site.register(Status, StatusModelAdmin)