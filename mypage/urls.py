from django.urls import path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import url
from .views import index

urlpatterns = [
    path('', index, name='index'),
    url(r'^$', index, name='index'),
]

urlpatterns += staticfiles_urlpatterns()