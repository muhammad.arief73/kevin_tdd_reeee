from django.contrib.auth.models import AnonymousUser, User
from mypage.views import index, profile
from django.http import HttpRequest
from django.test import TestCase
from mypage.models import Status
from mypage.forms import Status_Form
from django.test.client import Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import unittest

# Create your tests here.
class MyPageTest(TestCase):
    def test_mypage_url_is_exist(self):
        response = Client().get('/mypage/')
        self.assertEqual(response.status_code, 200)
        
    def test_hello_message(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(text = "Sedang berada di Fasilkom")
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status,1)
        
    def test_form_validation_for_blank_items(self):
        form = Status_Form(data={'text': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['text'],
            ["This field is required."]
        )

    def test_can_save_a_POST_request(self):
        response = self.client.post('/mypage/add_status/',
                                   data={'text': 'status update'})
        counting_all_available_activity = Status.objects.all().count()
        
        self.assertEqual(counting_all_available_activity, 1)
        self.assertEqual(response.status_code, 302)
        
        new_response = self.client.get('/mypage/')
        html_response = new_response.content.decode('utf8')

        self.assertIn('status update', html_response)
        
    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
        
    def test_myName_text(self):
        request = HttpRequest()
        response = profile(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Kevin Luvian Herdianto', html_response)
        
class funcTest(TestCase):
    def setUp(self):
        self.browser = webdriver.Chrome()
        self.browser.get('localhost:8000')
        super(funcTest, self).setUp()
        
    def tearDown(self):
        self.browser.quit()
        super(funcTest, self).tearDown()
        
    def test_layout(self):        
        headerTitle = self.browser.find_element_by_id("header-title").text
        self.assertEqual('Kevin LH', headerTitle)
        
        articleTitle = self.browser.find_element_by_id("article-container").text
        self.assertIn('Hello, Apa kabar?', articleTitle)
        
    def test_style(self):        
        buttonElement = self.browser.find_element_by_id("button-input")
        buttonColor = buttonElement.value_of_css_property("color")
        self.assertEqual('rgba(255, 193, 7, 1)', buttonColor)
        
        footerElement = self.browser.find_element_by_id("footer")
        footerColor = footerElement.value_of_css_property("background-color")
        self.assertEqual('rgba(251, 212, 8, 1)', footerColor)
        
    def test_input_status(self):        
        form = self.browser.find_element_by_name('text')
        
        submit = self.browser.find_element_by_name('action')
        
        form.send_keys('Coba Coba Coy')
        
        submit.send_keys(Keys.RETURN)
        
        self.assertIn('Coba Coba Coy', self.browser.page_source)
        time.sleep(5)
        
    def test_change_theme(self):
        buttonThemeChanger = self.browser.find_element_by_id("button-theme-changer")
        navbarTop = self.browser.find_element_by_id("navbar-top")
        
        buttonThemeChanger.click()
        testBGTheme = navbarTop.value_of_css_property("background-color")
        self.assertEqual('rgba(0, 0, 0, 1)', testBGTheme)
        
        buttonThemeChanger.click()
        testBGTheme = navbarTop.value_of_css_property("background-color")
        self.assertEqual('rgba(34, 183, 229, 1)', testBGTheme)