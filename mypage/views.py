from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Status_Form
from .models import Status  

# Create your views here.

def index(request):
	all_status = Status.objects.all()
	form = Status_Form
	return render(request, 'index.html', {'Status': all_status, 'form': form, 'nbar': 'index'})

def profile(request):
	return render(request, 'profile.html', {'nbar': 'profile'})

def add_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		text = request.POST.get('text', False)
		text = Status(text=text)
		text.save()
		return redirect(index)
	else:
		return HttpResponseRedirect('/index/')

