from django import forms

class Status_Form(forms.Form):
	error_messages = {
	'required': 'This field is required',
	'invalid': 'is not valid',
	}

	attrs = {
	'class': 'form-control'
	}
	text = forms.CharField(label='status', required=True, max_length=300,
	widget=forms.TextInput(attrs=attrs))