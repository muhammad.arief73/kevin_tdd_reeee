"""newsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from mypage import views
from mypage.views import add_status
from mypage.views import index, profile
from django.conf.urls import url
from django.conf.urls.static import static
import mypage.urls as mypage
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('mypage/', include('mypage.urls')),
    path(r'index/', index, name = 'index'),
    path(r'profile/', profile, name = 'profile'),
    url(r'^$', index, name='index'),
    url(r'add_status/$', add_status, name='add_status'),
    url(r'^mypage/', include(('mypage.urls', 'mypage'), namespace='mypage')),
]

urlpatterns += staticfiles_urlpatterns()